// index.js
const express = require('express');
const app = express();
const path = require('path');
const router = express.Router();
const name = process.env.FIRST_NAME;

router.get('/',function(req,res){
  var name = process.env.FIRST_NAME;
    res.render(path.join(__dirname+'/views/index.ejs'), {name:name});
    console.log('Test ' + name);
  });

app.use('/', router);

app.listen(5000, () => console.log('Server is up and running'));

const mysql = require('mysql');
const connection = mysql.createConnection({
  host: 'mysql',
  user: 'root',
  password: 'password',
  port: '3306',
  database: 'sys'
});

connection.connect((err) => {
  if (err) throw err;
  console.log('Connected to MySQL Server!');
  //If Required Table:
    var sql = "CREATE TABLE if not exists counter (count int)";
      connection.query(sql, function (err, result) {
        if (err) {
          console.log(err);
        }
        console.log("New table created");
      }); 
});