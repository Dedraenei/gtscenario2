# GTScenario2
#Create Docker Image for JS
docker build -t dedraenei/gtapp .
docker push dedraenei/gtapp:latest

#Run Deployments for Kubernetes
kubectl apply -f db-storage.yml
kubectl apply -f db-deployment.yml
kubectl apply -f js-deployment.yml

#Monitoring 
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add stable https://charts.helm.sh/stable
helm repo update
helm install prometheus prometheus-community/kube-prometheus-stack

port forward to view Grafana UI

#Stop Monitoring
helm delete prometheus 

#Stop Kubernetes Deployments
kubectl delete -f db-deployment.yml
kubectl delete -f db-storage.yml
kubectl delete -f js-deployment.yml